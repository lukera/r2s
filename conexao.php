<?php

define('HOST', 'localhost');    //Host
define('BANCODEDADOS', 'r2s');  //Nome do Banco de Dados
define('USUARIO', 'root');      //Usuario do Banco de Dados
define('SENHA', '');            //Senha do Banco de Dados

try {
    $conexao = new PDO("mysql:host=".HOST."; dbname=".BANCODEDADOS, USUARIO, SENHA);
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexao->exec("set names utf8");
} catch (PDOException $erro) {
    echo "Erro na conexão:" . $erro->getMessage();
}
