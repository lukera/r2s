<!doctype html>
<html lang="pt-br" class="h-100">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>R2S</title>
    <!-- Bootstrap core CSS -->
    <link href="_assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="_assets/dist/css/estilo.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">R2S</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Início <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente/cliente.php">Clientes</a>
                    </li>
                </ul>
                <!-- <form class="form-inline mt-2 mt-md-0">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Sair</button>
                </form> -->
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0">
        <div class="container">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Clientes</h5>
                    <p class="card-text">Cadastro e manutenção de clientes.</p>
                    <a href="cliente/cliente.php" class="card-link">Visualizar</a>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer mt-auto py-3">
        <div class="container">
            <span class="text-muted">Lucas Assunção Côrtes.</span>
        </div>
    </footer>
    <script src="_assets/dist/js/jquery-3.5.1.min.js"></script>
    <script src="_assets/dist/js/bootstrap.min.js"></script>

</html>