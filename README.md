# README #

* Sistema de Cadastro de Cliente para Restaurante
* Versão: 20200814

### Configuração ###

* Criar Banco de Dados: Query para criação do banco de dados e as tabelas necessarias no arquivo bd.sql
* Configurar conexão com Banco de Dados: Definir as constantes de conexão no arquivo conexao.php
* Para acessar a aplicação: Acessar navegador http://localhost/

### Desenvolvido por ###

* Lucas Assunção Côrtes