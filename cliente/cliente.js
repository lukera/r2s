var contato = {};
var contatos = [];

$(function () {

    tabela_cliente();

    $('#botao_modal_adicionar_cliente').on('click', function () {
        habilitar(true);
    });

    $('#botao_adicionar_contato').on('click', function () {
        if ($('#contato').val() !== "") {
            $("#tablela_contato tbody").append("<tr><td >" + $('#tipo_contato').val() + "</td><td>" + $('#contato').val() + "</td></tr>");
            contato.tipo_contato = $('#tipo_contato').val();
            contato.contato = $('#contato').val();
            $('#contato').val("")
            contatos.push(contato);
            contato = {};
        }
    });

    $('#botao_salvar').on('click', function () {
        if ($('#tipo_pessoa').val() === null) {
            $('#tipo_pessoa').addClass("is-invalid");
            $('#tipo_pessoa').focus();
        } else if ($('#cpf_cnpj').val() === "") {
            $('#tipo_pessoa').removeClass("is-invalid");
            $('#cpf_cnpj').addClass("is-invalid");
            $('#cpf_cnpj').focus();
        } else if ($('#nome').val() === "") {
            $('#cpf_cnpj').removeClass("is-invalid");
            $('#nome').addClass("is-invalid");
            $('#nome').focus();
        } else {
            $('#nome').removeClass("is-invalid");
            salvar_cliente($('#id_cliente').val());
        }
    });

    $('#modal_cliente').on('hidden.bs.modal', function (e) {
        $('#id_cliente').val("");
        $('#nome').val("");
        $('#tipo_pessoa').val(null);
        $('#cpf_cnpj').val("");
        $("#tablela_contato tbody").html("");
    });
});

function tabela_cliente() {
    var tabela_cliente = $('#tabela_cliente').DataTable({
        "ajax": {
            "url": "select.php",
            "dataSrc": "",
        },
        "columns": [
            { "title": "Código", "data": "id_cliente" },
            { "title": "Nome", "data": "nome" },
            { "title": "Tipo Pessoa", "data": "tipo_pessoa" },
            { "title": "CPF/CNPJ", "data": "cpf_cnpj" },
            { "title": "Ações", "data": "acao" }
        ],
        "order": [[1, "asc"]]
    });
    var callback_tabela_cliente = function (e, settings, json, xhr) {
        json.map(function (row) {
            row.acao = '<div class="dropdown">' +
                '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>' +
                '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">' +
                '<a class="dropdown-item" href="#" onClick="visualizar_cliente(' + row.id_cliente + ', false); return false;">Visualizar</a>' +
                '<a class="dropdown-item" href="#" onClick="editar_cliente(' + row.id_cliente + '); return false;">Editar</a>' +
                '<a class="dropdown-item" href="#" onClick="excluir_cliente(' + row.id_cliente + '); return false;">Excluir</a>' +
                '</div>' +
                '</div>'
        });
    }
    tabela_cliente.on('xhr.dt', callback_tabela_cliente);
}

function salvar_cliente(id_cliente) {
    contatos = contatos.length === 0 ? 0 : contatos;
    if (id_cliente === "") {
        $.ajax({
            type: "POST",
            url: "insert.php",
            data: {
                nome: $('#nome').val(),
                tipo_pessoa: $('#tipo_pessoa').val(),
                cpf_cnpj: $('#cpf_cnpj').val(),
                contatos: contatos
            },
            dataType:'json'
        }).done(function (data) {
            if (data.length > 0) {
                $('#modal_cliente').modal('hide');
                contatos = [];
                tabela_cliente();
                mensagem('Cliente inserido com sucesso', true);
            }
        }).fail(function (data) {
            if (data.length > 0) {
                //Mensagem de erro!
                mensagem('ERRO ao inserir Cliente( ' + data + ')', false);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: "update.php",
            data: {
                id_cliente: id_cliente,
                nome: $('#nome').val(),
                tipo_pessoa: $('#tipo_pessoa').val(),
                cpf_cnpj: $('#cpf_cnpj').val(),
                contatos: contatos
            },
            dataType:'json'
        }).done(function (data) {
            if (data.length > 0) {
                $('#modal_cliente').modal('hide');
                contatos = [];
                tabela_cliente();
                mensagem('Cliente atualizado com sucesso', true);
            }
        }).fail(function (data) {
            if (data.length > 0) {
                //Mensagem de erro!
                mensagem('ERRO ao atualizar Cliente( ' + data + ')', false);
            }
        });
    }
}

function visualizar_cliente(id_cliente, habilitar_) {
    habilitar(habilitar_);
    $.ajax({
        type: "GET",
        url: "select_cliente.php?id_cliente=" + id_cliente,
        dataType:'json'
    }).done(function (data) {
        if (data.length > 0) {
            $('#id_cliente').val(data[0].id_cliente);
            $('#nome').val(data[0].nome);
            $('#tipo_pessoa').val(data[0].tipo_pessoa);
            $('#cpf_cnpj').val(data[0].cpf_cnpj);
            visualizar_contato(id_cliente, habilitar_)
            $('#modal_cliente').modal('show');
        }
    }).fail(function (data) {
        if (data.length > 0) {
            mensagem('ERRO ao visualizar Cliente(' + data + ')', false);
        }
    });
}

function visualizar_contato(id_cliente, habilitar_) {
    $("#tablela_contato tbody").html('');
    $.ajax({
        type: "GET",
        url: "select_cliente_contato.php?id_cliente=" + id_cliente,
        dataType:'json'
    }).done(function (data_) {
        if (data_.length > 0) {
            $(data_).each(function (index, value) {
                $("#tablela_contato tbody").append(
                    "<tr>" +
                    "<td >" +
                    value.tipo_contato +
                    "</td>" +
                    "<td>" +
                    value.contato +
                    "</td>" +
                    "<td>" +
                    (habilitar_ ? '<div class="botao_excluir_contato"><button type="button" onClick="excluir_contato(' + value.id + ',' + id_cliente + '); return false;" class="btn btn-sm btn-outline-danger ">Excluir</button></div>' : '') +
                    "</td>" +
                    "</tr>"
                );
            });
        }
    }).fail(function (data) {
        if (data.length > 0) {
            mensagem('ERRO ao visualizar Contato(' + data + ')', false);
        }
    });

}

function editar_cliente(id_cliente) {
    visualizar_cliente(id_cliente, true);
}

function excluir_cliente(id_cliente) {
    $.ajax({
        type: "POST",
        url: "delete_cliente.php",
        data: {
            id_cliente: id_cliente
        },
        dataType:'json'
    }).done(function (data) {
        if (data.length > 0) {
            tabela_cliente();
            mensagem('Cliente excluído com sucesso', true);
        }
    }).fail(function (data) {
        if (data.length > 0) {
            mensagem('ERRO ao excluir Cliente( ' + data + ')', false);
        }
    });
}

function excluir_contato(id, id_cliente) {
    $.ajax({
        type: "POST",
        url: "delete_contato.php",
        data: {
            id: id
        },
        dataType:'json'
    }).done(function (data) {
        if (data.length > 0) {
            visualizar_contato(id_cliente, true);
        }
    }).fail(function (data) {
        if (data.length > 0) {
            mensagem('ERRO ao excluir Cliente( ' + data + ')', false);
        }
    });
}

function habilitar(habilitar) {
    $('#nome').attr('disabled', !habilitar);
    $('#tipo_pessoa').attr('disabled', !habilitar);
    $('#cpf_cnpj').attr('disabled', !habilitar);
    $('#tipo_pessoa').attr('disabled', !habilitar);
    $('#tipo_contato').attr('disabled', !habilitar);
    $('#contato').attr('disabled', !habilitar);
    $('#botao_adicionar_contato').attr('disabled', !habilitar);
    if (!habilitar) {
        $('#botao_salvar').hide();
        $('#div_contato').hide();
    } else {
        $('#botao_salvar').show();
        $('#div_contato').show();
    }
}

function mensagem(mensagem, sucesso) {
    $('#mensagem').append(
        '<div class="alert alert-' + (sucesso ? 'success' : 'danger') + '" role="alert">' +
        mensagem +
        '</div>'
    );
    $('#mensagem').focus();
    setTimeout(function () { $('#mensagem').html(''); }, 15000);
}