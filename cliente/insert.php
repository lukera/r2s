<?php

include('../conexao.php');

$nome = $_POST['nome'];
$tipo_pessoa = $_POST['tipo_pessoa'];
$cpf_cnpj = $_POST['cpf_cnpj'];
$contatos = $_POST['contatos'];

try {

    $sql = "INSERT INTO FAT_CLIENTE(nome, tipo_pessoa, cpf_cnpj) VALUES(:nome,:tipo_pessoa,:cpf_cnpj)";

    $stmt = $conexao->prepare($sql);
    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':tipo_pessoa', $tipo_pessoa);
    $stmt->bindParam(':cpf_cnpj', $cpf_cnpj);

    if ($stmt->execute()) {

        $id_cliente = $conexao->lastInsertId();

        if ($contatos == 0) {
            echo json_encode("Inserido com sucesso");
        } else {
            $values = array();

            foreach ($contatos as $row) {
                $values[] = '(' . $id_cliente . ',"' . $row['tipo_contato'] . '", "' . $row['contato'] . '")';
            }

            $values_ = implode(',', $values);

            try {

                $sql_ = "INSERT INTO FAT_CLIENTE_CONTATO(id_cliente, tipo_contato, contato) VALUES" . $values_;

                $stmt_ = $conexao->prepare($sql_);

                if ($stmt_->execute()) {
                    echo json_encode("Inserido com sucesso");
                } else {
                    throw new PDOException("Erro: Não foi possível executar a declaração sql");
                }
            } catch (PDOException $erro) {
                echo json_encode("Erro: " . $erro->getMessage());
            }
        }
    } else {
        throw new PDOException("Erro: Não foi possível executar a declaração sql");
    }
} catch (PDOException $erro) {
    echo json_encode("Erro: " . $erro->getMessage());
}
