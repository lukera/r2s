<?php

include('../conexao.php');

$id_cliente = $_POST['id_cliente'];

try {
    $sql = "DELETE FROM FAT_CLIENTE_CONTATO WHERE id_cliente=:id";
    $stmt = $conexao->prepare($sql);
    $stmt->bindParam(':id', $id_cliente);
    if ($stmt->execute()) {
        try {
            $sql_ = "DELETE FROM FAT_CLIENTE WHERE id_cliente=:id";
            $stmt_ = $conexao->prepare($sql_);
            $stmt_->bindParam(':id', $id_cliente);
            if ($stmt_->execute()) {
                echo json_encode("Excluido com sucesso");
            } else {
                echo "Erro: Não foi possível recuperar os dados do banco de dados";
            }
        } catch (PDOException $erro) {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } else {
        echo json_encode("Erro: Não foi possível recuperar os dados do banco de dados");
    }
} catch (PDOException $erro) {
    echo json_encode("Erro: " . $erro->getMessage());
}
