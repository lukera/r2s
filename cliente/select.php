<?php

include('../conexao.php');

try {
    $stmt = $conexao->prepare("SELECT * FROM FAT_CLIENTE");
    if ($stmt->execute()) {
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($results);
    } else {
        throw new PDOException("Erro: Não foi possível executar a declaração sql");
    }
} catch (PDOException $erro) {
    echo json_encode("Erro: " . $erro->getMessage());
}
