<?php

include('../conexao.php');

$id = $_POST['id'];

try {
    $sql = "DELETE FROM FAT_CLIENTE_CONTATO WHERE id=:id";
    $stmt = $conexao->prepare($sql);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        echo json_encode("Excluido com sucesso");
    } else {
        throw new PDOException("Erro: Não foi possível executar a declaração sql");
    }
} catch (PDOException $erro) {
    echo json_encode("Erro: " . $erro->getMessage());
}
