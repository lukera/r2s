<?php
include('../conexao.php');
?>
<!doctype html>
<html lang="pt-br" class="h-100">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>R2S | Clientes</title>
    <link href="../_assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../_assets/dist/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../_assets/dist/css/estilo.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">R2S</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="../index.php">Início </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="cliente.php">Clientes <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <!-- <form class="form-inline mt-2 mt-md-0">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Sair</button>
                </form> -->
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0">
        <div class="container">
            <div id="mensagem"></div>
            <div class="card">
                <div class="card-header">
                    <h4 class="d-inline">Tabela de Clientes</h4>
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal_cliente" id="botao_modal_adicionar_cliente">
                        Adicionar Cliente</button>
                </div>
                <div class="card-body">
                    <table id="tabela_cliente" class="table table-striped table-bordered" style="width:100%">
                    </table>
                </div>
            </div>
            <div class="modal fade" id="modal_cliente" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <form id="formulario_cliente" method="POST" action="insert.php">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="id_cliente">Código</label>
                                            <input type="text" class="form-control" id="id_cliente" disabled>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="tipo_pessoa">Tipo Pessoa</label>
                                            <select id="tipo_pessoa" name="tipo_pessoa" class="custom-select ">
                                                <option selected disabled>Selecionar...</option>
                                                <option value="F">Física</option>
                                                <option value="J">Jurídica</option>
                                            </select>
                                            <div id="tipo_pessoa_erro" class="invalid-feedback">
                                                Selecionar Tipo Pessoa!
                                            </div>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="cpf_cnpj">CPF/CNPJ</label>
                                            <input type="text" class="form-control " id="cpf_cnpj" name="cpf_cnpj" placeholder="Digite o CPF/CNPJ..." required>
                                            <div id="tipo_pessoa_erro" class="invalid-feedback">
                                                Preencher CPF/CNPJ!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" class="form-control " id="nome" name="nome" placeholder="Digite o nome..." required>
                                        <div id="nome_erro" class="invalid-feedback">
                                            Preencher Nome!
                                        </div>
                                    </div>
                                    <div class="form-row" id="div_contato">
                                        <div class="form-group col-md-12">
                                            <label>Contato</label>
                                            <div class="input-group">
                                                <select id="tipo_contato" class="form-control col-md-3">
                                                    <option value="Telefone">Telefone</option>
                                                    <option value="Email">Email</option>
                                                    <option value="Celular">Celular</option>
                                                </select>
                                                <input type="text" id="contato" class="form-control col-md-9">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-primary" type="button" id="botao_adicionar_contato">Adicionar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table" id="tablela_contato">
                                        <thead>
                                            <tr>
                                                <th scope="col">Tipo Contato</th>
                                                <th scope="col">Contato</th>
                                                <th scope="col" style="width: 15px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" id="botao_salvar" class="btn btn-outline-success">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer mt-auto py-3">
        <div class="container">
            <span class="text-muted">Lucas Assunção Côrtes.</span>
        </div>
    </footer>
    <script src="../_assets/dist/js/jquery-3.5.1.min.js"></script>
    <script src="../_assets/dist/js/popper.min.js"></script>
    <script src="../_assets/dist/js/bootstrap.min.js"></script>
    <script src="../_assets/dist/js/jquery.dataTables.min.js"></script>
    <script src="../_assets/dist/js/dataTables.bootstrap4.min.js"></script>
    <script src="../_assets/dist/js/script.js"></script>
    <script src="cliente.js"></script>

</html>