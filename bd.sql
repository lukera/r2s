-- Criar Banco de Dados
CREATE DATABASE `R2S`;

-- Selecionar Bando de Dados
USE `R2S`;

-- Criar tabela de clientes
CREATE TABLE `R2S`.`FAT_CLIENTE` (
    `id_cliente` INT NOT NULL AUTO_INCREMENT ,
    `nome` VARCHAR(80) NOT NULL ,
    `tipo_pessoa` VARCHAR(1) NOT NULL ,
    `cpf_cnpj` VARCHAR(20) NOT NULL ,
    PRIMARY KEY (`id_cliente`)
);

-- Inserir cliente inicial
INSERT INTO `R2S`.`FAT_CLIENTE` (`nome`, `tipo_pessoa`, `cpf_cnpj`) VALUES ('Lucas', 'F', '123.123.123-12');

-- Cirar tabela de contatos
CREATE TABLE `R2S`.`FAT_CLIENTE_CONTATO` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `id_cliente` INT NOT NULL ,
    `tipo_contato` VARCHAR(10) NOT NULL ,
    `contato` VARCHAR(80) NOT NULL ,
    PRIMARY KEY (`id`),
    INDEX `fk_idx` (`id_cliente` ASC),
    CONSTRAINT `id_cliente`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `FAT_CLIENTE`(`id_cliente`)
);

-- Inserir contatos
INSERT INTO `R2S`.`FAT_CLIENTE_CONTATO` (`id_cliente`, `tipo_contato`, `contato`) VALUES (1, 'CELULAR', '(34)99324-1288'),(1, 'EMAIL', 'LUCAS.CORTES@OUTLOOK.COM');
